FROM jupyter/tensorflow-notebook:python-3.8.8

USER root

# Install ETHZ root ca certificates
ADD http://pkiaia.ethz.ch/aia/ETHZRootCA2020.pem /usr/local/share/ca-certificates/ETHZRootCA2020.crt
ADD http://pkiaia.ethz.ch/aia/ETHZIssuingCA2020.pem /usr/local/share/ca-certificates/ETHZIssuingCA2020.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootCA.pem /usr/local/share/ca-certificates/DigiCertGlobalRootCA.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertTLSRSASHA2562020CA1-1.pem /usr/local/share/ca-certificates/DigiCertTLSRSASHA2562020CA1-1.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootG2.pem /usr/local/share/ca-certificates/DigiCertGlobalRootG2.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalG2TLSRSASHA2562020CA1.pem /usr/local/share/ca-certificates/DigiCertGlobalG2TLSRSASHA2562020CA1.crt
RUN chmod 644 /usr/local/share/ca-certificates/* && update-ca-certificates

# OS update, basic addon commands
RUN apt-get update && apt-get upgrade -y && \
  apt-get install -y python3-pip texlive-xetex lmodern zip less vim man gpg && \
  apt-get clean && \
  chown -R 1000 /opt/conda

# Create a directory for the NAS mount
RUN mkdir /data

# Export some conda variables
RUN echo 'export CONDA_ENVS_PATH=$HOME/.conda/envs CONDA_PREFIX=/opt/conda CONDA_AUTO_ACTIVATE_BASE=true' >> /etc/profile.d/conda_fix.sh && \
    echo 'conda init bash > /dev/null 2>&1' >> /etc/profile.d/conda_fix.sh && \
    echo 'if [ -f $HOME/.bashrc ]; then source $HOME/.bashrc; fi' >> /etc/profile.d/conda_fix.sh

# Fix timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >> /etc/timezone

# Switch the user - if we set the user at the very end rather than here, we need to chown again all the files
# created by the commands below
USER 1000

# Install QIIME 2 with all its dependencies + some JLab goodies
COPY qiime2-2023.2-py38-linux-conda.yml .
RUN mamba install -y -n base -c conda-forge -c defaults mamba=1.1.0 "jupyterhub>=3.0.0"
RUN mamba env update -n base --file qiime2-2023.2-py38-linux-conda.yml
RUN mamba install -y -n base \
      -c conda-forge -c bioconda -c https://packages.qiime2.org/qiime2/2023.2/tested -c gavinmdouglas \
      q2-types-genomics q2-picrust2 ncbi-datasets-pylib xmltodict gh \
      jupyterlab-system-monitor jupyterlab-git \
      jupyter_contrib_nbextensions webcolors uri-template isoduration fqdn \
      uncertainties nbgrader pylatex biopython ete3 nbgitpuller && \
    pip install git+https://github.com/bokulich-lab/RESCRIPt.git cython empress && \
    rm qiime2-2023.2-py38-linux-conda.yml && \
    jupyter serverextension enable --py qiime2 --sys-prefix && \
    qiime dev refresh-cache

# Add MOSHPIT dependencies
RUN mamba install -y -n base \
      -c conda-forge -c bioconda -c https://packages.qiime2.org/qiime2/2023.5/tested \
      q2-assembly q2-moshpit q2-fondue q2templates \
      kraken2 bracken "eggnog-mapper>=2.1.10" diamond tqdm xmltodict sourmash "busco >=5.0.0" "altair >=5" && \
    pip install git+https://github.com/dib-lab/q2-sourmash && \
    conda clean --all -y

# temporarily install the patched version of QUAST
# for whatever reason, this does not work with pip directly - need to clone first
RUN git clone https://github.com/misialq/quast.git /tmp/quast
WORKDIR /tmp/quast
RUN git checkout issue-230 && mamba run -n base pip install .

# temporarily update q2-moshpit/q2-types-genomics/q2-types to the most recent version
RUN git clone https://github.com/qiime2/q2-types.git /tmp/q2-types
WORKDIR /tmp/q2-types
RUN git checkout c2fc4de && mamba run -n base pip install .

RUN git clone https://github.com/bokulich-lab/q2-types-genomics.git /tmp/q2-types-genomics
WORKDIR /tmp/q2-types-genomics
RUN git checkout 6aa91a4 && mamba run -n base pip install .

RUN git clone https://github.com/bokulich-lab/q2-moshpit.git /tmp/q2-moshpit
WORKDIR /tmp/q2-moshpit
RUN git checkout ca9f5bb && mamba run -n base pip install .

RUN git clone https://github.com/bokulich-lab/q2-assembly.git /tmp/q2-assembly
WORKDIR /tmp/q2-assembly
RUN git checkout 634a05b && mamba run -n base pip install .

# clean up
RUN rm -r /tmp/quast /tmp/q2-types /tmp/q2-types-genomics /tmp/q2-moshpit /tmp/q2-assembly

WORKDIR $HOME

# Disable the "announcements" Jupyter extension
RUN jupyter labextension disable "@jupyterlab/apputils-extension:announcements"

# Enable the QIIME 2 extension
RUN CONDA_PREFIX=/opt/conda jupyter serverextension enable --py qiime2 --sys-prefix

# Add the CONDA_PREFIX variable to the kernel spec for QIIME2 to work properly
RUN jq '.env.CONDA_PREFIX="/opt/conda"' /opt/conda/share/jupyter/kernels/python3/kernel.json > temp.json && \
    mv temp.json /opt/conda/share/jupyter/kernels/python3/kernel.json

# Change the name of the default kernel
RUN jq '.display_name="QIIME 2"' /opt/conda/share/jupyter/kernels/python3/kernel.json > temp.json && \
    mv temp.json /opt/conda/share/jupyter/kernels/python3/kernel.json \
